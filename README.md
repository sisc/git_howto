# git_howto

git_howto for project work

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [git_howto](#githowto)
- [Disclaimer](#disclaimer)
- [First time: clone repo, setup git config](#first-time-clone-repo-setup-git-config)
- [Golden Rules](#golden-rules)
- [Basic git workflow in a terminal](#basic-git-workflow-in-a-terminal)
- [Branching](#branching)

<!-- markdown-toc end -->


# Disclaimer #

This How-to was tested with and should work on remote git.rwth-aachen.de and
locals hpc.itc.rwth-aachen.de, vispa.physik.rwth-aachen.de, and personal
computer.

# First time: clone repo, setup git config #

Go to the repository's homepage. On the top right, there is usually a 'clone' button. Copy the HTTPS address.

Open a terminal. Clone the remote repository to somewhere inside your local
drive / cluster home directory.

``` shell
# first time: clone repo:
git clone https://git.rwth-aachen.de/pathto/myproject.git

# enter the clone = local git repo:
# (note: you can move this folder wherever you like anytime. it is self-contained.)
cd myproject

# once inside, git recognizes this folder as a repo by the presence of a .git subfolder.
# display git commands (note: git has tab completion)
git help
```

Important: Provide your name and mail address, so that in the shared repo, the
changes you make get associated with your name later on.

``` shell
# Open the command line.
# show git config. of our concern right now: the user entries.
git config --list
# Set your username (--global: for all your repos; without: only for this repo):
git config --global user.name "firstname lastname"
# Set your email address:
git config --global user.email "myname@example.com"
# check that it is set
git config --global user.name
git config --global user.email
```

Optional: store your login so you don't have to type it in on every push/pull
to/from remote repo.

``` shell
# (without --global: only for this repo)
git config --global credential.helper store
# then on next
git pull
# or push with changes: provide username and password
```

# Golden Rules #

1. don't push buggy code. only push running code (e.g. selective git add, ...).
2. prefer many small commits to one large commit. that is the whole point of
   version control.
3. Always: first PULL then PUSH.

# Basic git workflow in a terminal #

``` shell
# show latest local changes:
git status

# Now EITHER add all changes, if all thematically related... 
git add -A
# ... OR add only important changes (generally preferred):
git add file1 folder1/file2
git add folder1/file3

# Now pack the changes into one commit defining that change:
git commit -m "short one-liner explaining change"

# now either return to coding, accumulating more commits before pushing, 
# or push this single commit immediately to remote repo:
git pull # ALWAYS before push 
git push

# optional: check in browser if commit has arrived in remote repo
```

Note: `git push` is an abbreviation for `git push origin master`, where `origin`
means 'the central remote repo' and `master` means the master branch = main branch.

Now after the `git pull`, if someone else has been working on the same branch as
you and pushed changes, it **may** sometimes happen that these changes conflict
with your local unpushed changes. In that case, git will tell you there are
unresolved conflicts. Then you have to manually resolve the conflicts. Some
helpful tools: [kdiff3](http://kdiff3.sourceforge.net/) (do: open conflicting
file versions and select 'merge'), [meld](http://meldmerge.org/), or even
[MATLAB](https://www.mathworks.com/help/matlab/matlab_prog/resolve-source-control-conflicts.html).
If you only have the command line, this might get difficult without knowledge of
vim or emacs, though. 

Once you resolved all conflicts locally, you can proceed to `git push`.

# Branching #

The idea of branching is that different team members may want to make their own
versions of the code, or work on a different feature. For this, one creates a
`branch` in which all changes are isolated from other branches. Once one is done
with that work, one can `merge` the changes back into the master branch (or the
trunk) to share it with the other team members. In fact, the best practice is to
never work directly in the `master` branch, but only use it for merging.

``` shell
# show all branches, local and remote:
# (note: the branch you're on is marked by an asterisk *)
git branch -av

# switch to another branch (with '-b': create locally if not exist):
git checkout -b mybranch # e.g. in small group, use your first name

# optional: push branch to remote repo:
git push origin mybranch

# optional: switch to remote branch:
# say you want to work on that branch in two different locals
# (eg on your computer and in your home on some cluster env),
# or work on branch of someone else.
# then have to push branch as above and checkout at other local:
git fetch
git checkout mybranch

# work in branch (add, commit, push origin mybranch) 

# work done: merge mybranch into -> master branch:
git checkout master
git pull
# (optional: show differences: git diff mybranch master)
git merge mybranch

# resolve conflicts if any, then:
git push
# return to branch: done.
git checkout mybranch
```

Note: when pushing on your own branch you need to explicitly state `git push
origin mybranch`, because `git push` would try to push to `origin master` which
you normally don't want.


More info:
- [git simple guide](https://rogerdudler.github.io/git-guide/)
- google
